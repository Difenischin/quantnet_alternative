import os
import requests
import json
import pandas as pd


def get_authorization_token():
    absolute_script_dir = os.path.dirname(__file__)
    file_user_login_and_password = "ENV.txt"
    abs_file_path = os.path.join(absolute_script_dir, file_user_login_and_password)
    user_password_grands = open(abs_file_path).read()
    headers = {

        'authorization': 'Basic Y2xpZW50OnNlY3JldA==',
        'content-type': 'application/x-www-form-urlencoded',

    }

    r = requests.post('https://quantnet.ai/oauth/token', headers=headers, data=user_password_grands)

    result = r.content.decode('utf-8')
    json_result = json.loads(result)
    return json_result['access_token']


def load_all_strategies(access_token):
    headers = get_headers(access_token)

    r = requests.get('https://quantnet.ai/referee/submission/', headers=headers)

    result = r.content.decode('utf-8')
    json_result = json.loads(result)
    return json_result


def get_headers(access_token):
    return {
        'accept': '*/*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        'authorization': 'Bearer ' + access_token,
        'content-type': 'application/json;charset=UTF-8',
        'cookie': 'utm={}; language=ru; color=%23f16c20; qn_uid=1592; access_token=22441e42-bef3-4325-bdc7-bca131af9f4f; username=Difenischin; JSESSIONID=JSESSIONID=AC6B856A8F9548515848D688078091B3',
        'referer': 'https://quantnet.ai/personalpage/submissions',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
    }


def get_load_strategy_competitions(strategy_id, access_token):
    headers = get_headers(access_token)
    url = 'https://quantnet.ai/referee/submission/' + str(strategy_id) + '/competitions'
    r = requests.get(url, headers=headers)

    result = r.content.decode('utf-8')
    json_result = json.loads(result)
    return json_result


def get_competitions_strategy_all(access_token):
    strategy_all = load_all_strategies(access_token)
    strategies_with_competitions = []
    for strategy in strategy_all:
        competitions = get_load_strategy_competitions(strategy['id'], access_token)
        for competition in competitions:
            competition['strategy_id'] = strategy['id']
            competition['name'] = strategy['name']
            competition['inSmpl'] = strategy['inSmpl']
            competition['outSmpl'] = strategy['outSmpl']
            strategies_with_competitions.append(competition)
    return strategies_with_competitions


def get_competitions_strategy_all_df_active(access_token):
    ids_strategy = []
    ids_competitions = []
    strategy_names = []
    ends = []
    durations = []
    ranks = []
    scores = []
    statuses = []
    prizes = []
    sharpe_ins = []
    sharpe_outs = []
    competitions_strategy_all = get_competitions_strategy_all(access_token)
    for order in competitions_strategy_all:
        ids_strategy.append(order['strategy_id'])
        ids_competitions.append(order['id'])
        strategy_names.append(order['name'])
        ends.append(order['end'])
        durations.append(order['phaseDuration'])
        ranks.append(order['rank'])
        scores.append(order['score'])
        statuses.append(order['status'])
        prizes.append(order['prize'])
        sharpe_ins.append(order['inSmpl'])
        sharpe_outs.append(order['outSmpl'])

    df_all = pd.DataFrame()
    df_all['strategy_id'] = ids_strategy
    df_all['ids_competitions'] = ids_competitions
    df_all['name'] = strategy_names
    df_all['end'] = ends
    df_all['duration'] = durations
    df_all['rank'] = ranks
    df_all['status'] = statuses
    df_all['score'] = scores
    df_all['prize'] = prizes
    df_all['inSmpl'] = sharpe_ins
    df_all['outSmpl'] = sharpe_outs
    return df_all


def get_competitions_strategy():
    access_token = get_authorization_token()
    df = get_competitions_strategy_all_df_active(access_token)
    return df
