from flask import Flask
from flask_cors import CORS
from werkzeug.contrib.cache import SimpleCache
import quantnet_parser

default_timeout = 60 * 60  # 1 -sec
cache = SimpleCache()

app = Flask(__name__)
CORS(app)


def get_strategies_future_cache():
    print('get_strategies_future')
    cache_name = 'get_strategies_future'
    price_max_cache = cache.get(cache_name)
    if price_max_cache is None:
        strategies_df = quantnet_parser.get_competitions_strategy()
        strategies_df.to_csv("strategies_df.csv", ";", encoding='utf-8')
        strategies_running_df = strategies_df[strategies_df['status'] == 'RUNNING']
        sorted_df = strategies_running_df.sort_values(by=['end', 'rank'])
        strategies_json = sorted_df.to_json(orient='table', force_ascii=False)
        cache.set(cache_name, strategies_json, timeout=default_timeout)
        return strategies_json
    return price_max_cache


@app.route('/get_strategies_future', methods=['GET', 'POST'])
def get_strategies_future():
    return get_strategies_future_cache()


@app.route('/', methods=['GET', 'POST'])
def index():
    return "/get_strategies_future"


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5555)
