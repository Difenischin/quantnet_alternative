import React from 'react'
import {Container, Typography, Grid, Switch} from '@material-ui/core';
import MaterialTable from 'material-table';

import StrategyFilterDuration from './StrategyFilterDuration';
import StrategyFilterDateEndDuration from './StrategyFilterDateEndDuration'
import axios from "axios";

interface ContestFilters {
    dates: string[],
    durations: number[],
    statusContest: string[],
}


function isBestStrategy(rank: number) {
    return rank < 7;
}

function getMoneyFormat(money: number) {
    const formatter = new Intl.NumberFormat('ru');
    return formatter.format(money);
}

function getRoundFormat(num: number) {
    return Math.round(num * 100) / 100;
}

function getLikelyPrize(prizeFond: number, rank: number) {
    let rankCorrector = 0;
    if (rank === 1) {
        rankCorrector = 0.4;
    }
    if (rank === 2 || rank === 3) {
        rankCorrector = 0.15;
    }
    if (rank > 3 && rank <= 6) {
        rankCorrector = 0.1;
    }
    return Math.round(prizeFond * rankCorrector);
}


export default class StrategyCompetitionsFutureTable extends React.Component {
    state = {
        strategiesForSubmission: [],
        isShowBestStrategiesOnly: true,
        userFilterChoices: {
            dates: [],
            durations: [],
            statusContest: [],
        }
    };

    handleChangeShowBestStrategiesOnly = () => {
        this.setState({
            isShowBestStrategiesOnly: !this.state.isShowBestStrategiesOnly
        });
    }
    handleChangeShowDurations = (durationsChoicesUser: []) => {
        const curState = {...this.state}
        curState.userFilterChoices.durations = durationsChoicesUser;
        this.setState({
            curState
        });
    }
    handleChangeShowDateEnd = (dateEndChoicesUser: []) => {
        const curState = {...this.state}
        curState.userFilterChoices.dates = dateEndChoicesUser;
        this.setState({
            curState
        });
    }

    componentDidMount() {
        const host = window.location.hostname;
        const newHost = host + ':5555';
        const backendUrlForStrategies = 'http://' + newHost + '/get_strategies_future';
        axios.get(backendUrlForStrategies)
            .then(response => {
                const strategiesSorted = getSortedByDate(response.data.data);
                const contestFilters = getContestFilters(strategiesSorted);
                contestFilters.durations = contestFilters.durations.sort((x, y) => x - y);
                this.setState({
                    strategiesForSubmission: strategiesSorted,
                    isShowBestStrategiesOnly: true,
                    userFilterChoices: contestFilters
                })

            })
            .catch(error => console.error(error));

        function getSortedByDate(strategiesForSubmission: any) {
            return strategiesForSubmission.sort((a: any, b: any) => {
                return getIntForTextDate(a.end) - getIntForTextDate(b.end);
            });

            function getIntForTextDate(dateString: any) {
                const parts = dateString.split('-');
                const yyyymmdd = parts.reverse().join('');
                return +yyyymmdd;
            }
        }

        function getContestFilters(strategiesForSubmission: any) {
            const uniqueDates = new Set();
            const uniqueDurations = new Set();
            const uniqueStatusContest = new Set();
            for (const strategy of strategiesForSubmission) {
                if (typeof strategy.end !== undefined) {
                    const end: string = strategy.end;
                    uniqueDates.add(end);
                }
                if (typeof strategy.duration !== undefined) {
                    const duration: number = strategy.duration;
                    uniqueDurations.add(duration);
                }
                if (typeof strategy.status !== undefined) {
                    const status: string = strategy.status;
                    uniqueStatusContest.add(status);
                }
            }
            // @ts-ignore
            const dates: string[] = [...uniqueDates];
            // @ts-ignore
            const durations: number[] = [...uniqueDurations];
            // @ts-ignore
            const statusContest: string[] = [...uniqueStatusContest];
            const result: ContestFilters = {
                dates,
                durations,
                statusContest,
            }
            return result;
        }
    }

    render() {
        const strategyForShow = this.state.strategiesForSubmission.filter((strategy: any | never) => {
            // @ts-ignore
            const durationsFilter = this.state.userFilterChoices.durations.includes(strategy.duration);
            // @ts-ignore
            const datesFilter = this.state.userFilterChoices.dates.includes(strategy.end);
            let bestStrategyFilter = true;
            if (this.state.isShowBestStrategiesOnly) {
                bestStrategyFilter = isBestStrategy(strategy.rank);
            }
            return durationsFilter && datesFilter && bestStrategyFilter;
        })
        const potentialProfit = strategyForShow.reduce(function (accumulator: number, currentValue: any) {
            const {rank} = currentValue;
            if (!isBestStrategy(rank)) {
                return accumulator;
            }
            return accumulator + getLikelyPrize(currentValue.prize, rank);
        }, 0);

        const isFiltersExist = this.state.userFilterChoices.dates.length > 0;
        return (
            <Container>
                {isFiltersExist &&
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <StrategyFilterDateEndDuration dateEndDefault={this.state.userFilterChoices.dates}
                                                       handleChangeShowDateEnd={this.handleChangeShowDateEnd}/>
                    </Grid>
                    <Grid item xs={4}>
                        <StrategyFilterDuration durationsDefault={this.state.userFilterChoices.durations}
                                                handleChangeShowDurations={this.handleChangeShowDurations}/>

                    </Grid>
                    <Grid item xs={3}>
                        <Switch name='Show Best Strategy Only'
                                onChange={() => this.handleChangeShowBestStrategiesOnly()}
                                checked={this.state.isShowBestStrategiesOnly}
                                color="primary"
                        />Show Best Strategy Only
                        <Typography variant="subtitle1" gutterBottom>
                            You claim to win <strong>{getMoneyFormat(potentialProfit)}</strong> rubles. Given the
                            filters. Every day the market situation
                            is changing
                        </Typography>
                    </Grid>
                </Grid>
                }


                <MaterialTable
                    columns={[
                        {title: 'Name', field: 'name'},
                        {title: 'Date Contest End', field: 'end'},
                        {title: 'Duration', field: 'duration'},
                        {
                            title: 'OS', field: 'outSmpl', render: rowData => {
                                // @ts-ignore
                                return getRoundFormat(rowData.outSmpl)
                            }
                        },
                        {title: 'Rank', field: 'rank'},
                        {
                            title: 'Score', field: 'score', render: rowData => {
                                // @ts-ignore
                                return getRoundFormat(rowData.score)
                            }
                        },
                        {
                            title: 'Prize Fond',
                            field: 'prize',
                            render: rowData => {
                                // @ts-ignore
                                return getMoneyFormat(rowData.prize)
                            },
                            cellStyle: {textAlign: 'end'}
                        },
                        {
                            title: 'Likely prize',
                            field: 'prize',
                            render: rowData => {
                                // @ts-ignore
                                return getMoneyFormat(getLikelyPrize(rowData.prize, rowData.rank))
                            },
                            cellStyle: {textAlign: 'end'}
                        },
                    ]}
                    data={strategyForShow}
                    title="Current position of strategies in future competitions"
                    options={{
                        sorting: true,
                        pageSize: 25,
                        padding: 'dense',
                        rowStyle: rowData => {
                            if (isBestStrategy(rowData.rank)) {
                                return {
                                    backgroundColor: '#1fa21175'
                                }
                            }
                            return {
                                backgroundColor: (rowData.tableData.id % 2 === 0) ? '#EEE' : '#FFF'
                            }
                        },
                    }}
                />
            </Container>

        );
    }
}