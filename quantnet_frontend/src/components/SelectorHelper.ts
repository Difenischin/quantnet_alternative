export default class SelectorHelper {
    static getForChoice(arrays: any): { value: any, isChoice: boolean }[] {
        const result = [];
        for (const obj of arrays) {
            result.push({
                value: obj,
                isChoice: true
            })
        }
        return result;
    };

    static getForChoiceRevertIfTrue(arrays: any): [] {
        let result: any = [];
        for (const obj of arrays) {
            if (obj.isChoice) {
                result.push(obj.value);
            }
        }
        return result;
    }
}