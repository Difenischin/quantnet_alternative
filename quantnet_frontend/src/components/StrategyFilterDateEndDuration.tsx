import React from 'react'
import {FormControl, FormLabel, FormControlLabel, Radio, RadioGroup} from "@material-ui/core";


type StrategyFilterDateEndProps = {
    dateEndDefault: any,
    handleChangeShowDateEnd: any
}

export default class StrategyFilterDuration extends React.Component<StrategyFilterDateEndProps> {
    constructor(props: any) {
        super(props);

        this.state = {
            dateEndDefault: props.dateEndDefault,
            dateEndDurationForChoice: ['Next months', 'Next three months', 'Next six months', 'Next year'],
            userChoice: 'Next months'
        }
    }

    handleChangeShowDateEnd(endDuration: string) {
        this.setState({
            userChoice: endDuration
        });
        const countMonths = {
            'Next months': 1,
            'Next three months': 3,
            'Next six months': 6,
            'Next year': 12,
        }
        // @ts-ignore
        const dateEndChoices = this.state.dateEndDefault.slice(0, countMonths[endDuration]);
        this.props.handleChangeShowDateEnd(dateEndChoices);
    }

    render() {
        return (
            <div>
                <FormControl component="fieldset">
                    <FormLabel component="legend">End time of next competition</FormLabel>
                    <RadioGroup>
                        {
                            // @ts-ignore
                            this.state.dateEndDurationForChoice.map(endDuration => {
                                return (
                                    <FormControlLabel key={endDuration}
                                                      value={endDuration}
                                                      control={<Radio name={endDuration}
                                                                      onChange={() => this.handleChangeShowDateEnd(endDuration)}
                                                                      color="primary"
                                                      />}
                                                      label={endDuration}/>)
                            })
                        }
                    </RadioGroup>
                </FormControl>
            </div>
        )
    }
}