import React from 'react'

import {Container, Grid, Typography} from '@material-ui/core';
import MaterialTable from 'material-table';


export default class StrategyCompetitionsCurrentMonthTable extends React.Component {

    render() {
        return (
            <Container>
                <Grid container spacing={2}>
                    <Grid item xs={4}>
                        <Typography variant="subtitle1" gutterBottom>
                            You can send up to 20 strategies in the current month.
                            You have sent 20 of 20.
                        </Typography>
                    </Grid>
                </Grid>

                <MaterialTable
                    columns={[
                        {title: 'Name', field: 'name'},
                        {title: 'Date Contest End', field: 'end'},
                        {title: 'Duration', field: 'duration'},
                        {title: 'OS', field: 'outSmpl'},
                        {title: 'Rank', field: 'rank'},
                        {title: 'Score', field: 'score'},
                        {title: 'Prize', field: 'prize'},
                    ]}
                    data={[]}
                    title="Current position of strategies sent this Month"
                />
                <MaterialTable
                    columns={[
                        {title: 'Name', field: 'name'},
                        {title: 'Date Contest End', field: 'end'},
                        {title: 'Duration', field: 'duration'},
                        {title: 'OS', field: 'outSmpl'},
                        {title: 'Rank', field: 'rank'},
                        {title: 'Score', field: 'score'},
                        {title: 'Prize', field: 'prize'},
                    ]}
                    data={[]}
                    title="Checking strategies sent this Month"
                />
                <MaterialTable
                    columns={[
                        {title: 'Name', field: 'name'},
                        {title: 'Date Contest End', field: 'end'},
                        {title: 'Duration', field: 'duration'},
                        {title: 'OS', field: 'outSmpl'},
                        {title: 'Rank', field: 'rank'},
                        {title: 'Score', field: 'score'},
                        {title: 'Prize', field: 'prize'},
                    ]}
                    data={[]}
                    title="Problems strategies sent this Month"
                />
            </Container>

        );
    }
}